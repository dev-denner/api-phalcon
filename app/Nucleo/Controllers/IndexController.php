<?php

/**
 *
 * @author      Denner Fernandes <denners777@hotmail.com>
 * @since       30/06/2015 02:59:13
 *
 */
        

namespace Nucleo\Controllers;

use Phalcon\Mvc\Controller;

/**
 * Class IndexController
 * @package Nucleo\Controllers
 */
class IndexController extends Controller
{
    public function indexAction()
    {

    }
}